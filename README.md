The instructions contained here assume you're using systemd.

REQUIREMENTS:
jq
oathtool
sudo privileges

The purpose of this script is to allow you to connect to an OpenVPN Server that
requires MultiFactor Authentication using a TOTP Token without prompting the
user for the MFA Token.

How to use this script:

Create a json file in the location specified by ${MFAKEYFILE} such as:

{ "vpnname": "TOTP_SECRET",
  "vpnname2": "TOTP_SECRET2" }

Set permissions with chmod to only allow the owner to access it:
'chmod 600 ~/.mfakeys-openvpn'

This file shall contain a key:value pair for each openvpn server.
The 'key' is the part of the '.ovpn' filename without the extension.
The 'value' is the 'TOTP SECRET' you normally obtain by scanning a QR Code.

Copy your '.ovpn' file(s) to /etc/openvpn/client/
Rename the '.ovpn' file(s) to use a '.conf' extension.
Modify the '.conf' file, adding a filename after auth-user-pass:

Example: 'auth-user-pass vpnname-id'

Create '/etc/openvpn/client/vpnname-id'
Also restrict permissions on this file:
'chmod 600 /etc/openvpn/client/vpnname-id'

Each '.conf' file should use a different username/password file, so change
'vpnname-id' as appropriate.

The file should have two lines
  - First line is your VPN Username
  - Second line is your VPN Password

Finally, copy the vpnctl script from this repository to a directory contained
within your ${PATH}. I recommend '/usr/local/bin/'

This script can be used to stop and start the 'vpnname' profile without
prompting for the MFA TOTP token.

Example:
vpnctl start vpnname

This will start the VPN defined by the config file:
'/etc/openvpn/client/vpnname.conf'

If you have suggestions, please open an Issue on GitLab:
https://gitlab.com/TheLinuxNinja/vpnctl/issues
